Lecturers
 - Store student profiles
 - Track attendance through multiple classes
 - Add/remove students
 - View attendence stats
 - Mark students absent/present
 - Edit student attendance
 - View students under attendance threshold

Students
 - Student Number (View)
 - Student Password (View)
 - Mode of study (FT/PT) (View)
 - Degree type (View)
 - View personal attendance
 - Course (View)
