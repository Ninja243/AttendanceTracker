from modules.dbm import add
from modules.dbm import mark
from modules.dbm import view
from modules.dbm import purge
from modules.dbm import auth
import pymongo

connection = pymongo.MongoClient('mongodb://localhost')
db = connection.attendance_tracker_db
classes = db.classes
# log, find, auth


from flask import Flask, session, redirect, request, Response
import base64, sys

SECRET_KEY = 'akjsdghdskfhuvaehrunaoudfhvaetn'
SESSION_TYPE = 'SESSION_MONGODB'
app = Flask(__name__)
app.secret_key = 'akjsdghdskfhuvaehrunaoudfhvaetn'
app.config.from_object(__name__)
app.debug = True
app.run(debug=True)

# Imports :(
header = str(open("modules/lego/header.html", "r").read())
footer = str(open("modules/lego/footer.html", "r").read())
addcourse = str(open("modules/lego/addcourse.html", "r").read())
adduser = str(open("modules/lego/adduser.html", "r").read())
adminnav = str(open("modules/lego/adminnav.html", "r").read())
helloworld = str(open("modules/lego/helloworld.html", "r").read())
navclose = str(open("modules/lego/navclose.html", "r").read())
usernav = str(open("modules/lego/usernav.html", "r").read())
style = str(open("modules/lego/main.css", "r").read())
navopen = str(open("modules/lego/navopen.html", "r").read())
loginform = str(open("modules/lego/login.html", "r").read())
ma = str(open("modules/lego/matkatt.html", "r").read())
manage = str(open("modules/lego/manage.html", "r").read())

# TESTING
purge.allCourses()
purge.allStaff()
purge.allStudents()

def getStats(studentNumber):
	allClasses = classes.find({})
	i = 0
	j = 0
	for spclass in allClasses:
		if spclass['studentNumber'] == studentNumber:
			i = i+1
		j = j+1
	try:
		return i/j*100
	except:
		return 0

add.student("Stu218118139", "Mweya", "Ruider", "2")
add.staffMember("Sta218081030", "Shakeel", "Hoosain", False, "3")
add.course("DSP620S", "H. Kandjimi", "Dealing with Sharp Pain")

@app.route("/", methods=["GET"])
def a():
	if "studentNumber" in session:
		studentNumber = session["studentNumber"]
		username = view.specificStudent(session["studentNumber"])[0]
		return header+navopen+"<li class='rightli'><a href='/logout'>Log Out</a></li>"+navclose+"<h1>Hello"+username+"</h1><p>Choose one of the options above to get started.</p>"+footer
	elif "staffNumber" in session:
		staffNumber = session["staffNumber"]
		username = view.specificStaffMember(session["staffNumber"])[0]
		return header+navopen+"<li><a href='/mark'>Take attendance</a></li><li><a href='/manage'>Manage Users/Courses</a></li><li class='rightli'><a href='/logout'>Log Out</a></li>"+navclose+"<h1>Hello "+username+"</h1><p>Choose one of the options above to get started.</p>"+footer
	else:
		return header+navopen+"<li class='rightli'><a href='/login'>Log In</a></li>"+navclose+helloworld+footer

@app.route("/style", methods=["GET"])
def b():
	return Response(style, mimetype="text/css")

@app.route("/staff", methods=["GET"])
def c():
	toreturn = header+navopen+navclose
	#toreturn = toreturn+view.staff()
	j = 0
	for staff in view.allStaff():
		admin = ""
		print(staff['admin'])
		if str(staff['admin']) == "True":
			admin = "admin"
		toreturn = toreturn+"<div class='person staff "+admin+"'><a href='/s/"+staff['staffNumber']+"'><h4>"+staff['firstname']+" "+staff['lastname']+"</h4></a></div>"
		j = j+1
	if j == 0:
		toreturn = toreturn+"<h1>No staff members have been added yet.</h1><p>Add a staff member and try again.</p>"
	toreturn = toreturn+footer
	return Response(toreturn, mimetype="text/html")

@app.route("/students", methods=["GET"])
def d():
	toreturn = header+navopen+navclose
	j = 0
	for student in view.allStudents():
		toreturn = toreturn+"<div class='person student'><a href='/l/"+student['studentNumber']+"'><h4>"+student['firstname']+" "+student["lastname"]+"</h4></a></div>"
		j = j+1
	if j == 0:
		toreturn = toreturn+"<h1>No students have been added yet.</h1><p>Add a student and try again.</p>"
	toreturn = toreturn+footer
	return Response(toreturn, mimetype="text/html")

@app.route("/courses", methods=["GET"])
def e():
	toreturn = header+navopen+navclose
	j = 0
	for course in view.allCourses():
		toreturn = toreturn+"<div class='course'><a href='/c/"+course['courseCode']+"'><h4><i>"+course['name']+"</i> taught by "+course['lecturer']+"</h4></a></div>"
		j = j+1
	if j == 0:
		toreturn = toreturn+"<h1>No courses have been added yet</h1><p>Add a course and try again.</p>"
	toreturn = toreturn+footer
	return Response(toreturn, mimetype="text/html")

@app.route("/login", methods=["GET", "POST"])
def f():
	if request.method == "GET":
		toreturn = header+navopen+navclose+loginform+footer
		return Response(toreturn, mimetype="text/html")
	elif request.method == "POST":
		uname = request.form['username']
		pword = request.form['password']
		response = auth.login(uname, pword)
		session.modified = True
		if response == "Logged in student":
			session['studentNumber'] = uname
			print("Logged in")
		if response == "Logged in staff":
			session["staffNumber"] = uname
			print("Logged in")
		if response == ("Logged in admin"):
			session["staffNumber"] = uname
			print("Logged in")
		return redirect("/")

@app.route("/logout", methods=["GET"])
def g():
	session.pop('studentNumber', None)
	session.pop('staffNumber', None)
	return redirect("/")

@app.route("/s/<staffNumber>")
def h(staffNumber):
	firstname, lastname, admin = view.specificStaffMember(staffNumber)
	torespond = header+navopen+navclose+"<h1>"+firstname+" "+lastname+"</h1><i><p>"+staffNumber+"</p><p>Admin status: "+str(admin)+"</p></i>"+footer
	return Response(torespond, mimetype="text/html")

@app.route("/l/<studentNumber>")
def i(studentNumber):
	firstname, lastname = view.specificStudent(studentNumber)
	torespond = header+navopen+navclose+"<h1>"+firstname+" "+lastname+"</h1><i><p>"+studentNumber+"</p></i>"+"<h2>Attendance Percentage: "+str(getStats(studentNumber))+"%</h2>"+footer
	return Response(torespond, mimetype="text/html")

@app.route("/c/<courseCode>")
def j(courseCode):
	name, lecturer, students = view.specificCourse(courseCode)
	torespond = header+navopen+navclose+"<h1>"+name+"</h1><i><p>Taught by "+lecturer+"</p></i>"+footer
	return Response(torespond, mimetype="text/html")

@app.route("/mark", methods=["GET", "POST"])
def k():
	if request.method == "POST":
		if "staffNumber" in session:
			try:
				classes.insert_one({"courseCode":request.form['courseCode'], "studentNumber":request.form["studentNumber"], "classNumber":request.form['classNumber']})
				return redirect("/")
			except:
				return redirect("/error")
	else:
		if "staffNumber" in session:
			toreturn = header+navopen+"<li><a href='/mark'>Take attendance</a></li><li><a href='/manage'>Manage Users/Courses</a></li><li class='rightli'><a href='/logout'>Log Out</a></li>"+navclose
			toreturn = toreturn + ma+footer #form here
			return Response(toreturn, mimetype="text/html")

		else:
			return redirect("/")

@app.route("/manage", methods=["GET", "POST"])
def l():
	if request.method == "POST":
		if "staffNumber" in session:
			try:
				if request.form['type'] == "Student":
					add.student(request.form["studentNumber"], request.form["firstname"], request.form["lastname"], request.form["password"])
					return redirect("/students")
				elif request.form['type'] == "Staff":
					add.staffMember(request.form["staffNumber"], request.form["firstname"], request.form["lastname"], False, request.form["password"])
					return redirect("/staff")
				elif request.form['type'] == "Course":
					add.course(request.form["courseCode"], request.form["lecturer"], request.form["courseName"])
					return redirect("/courses")
			except:
				return redirect("/")
	else:
		if "staffNumber" in session:
			toreturn = header+navopen+"<li><a href='/mark'>Take attendance</a></li><li><a href='/manage'>Manage Users/Courses</a></li><li class='rightli'><a href='/logout'>Log Out</a></li>"+navclose
			toreturn = toreturn + manage+footer
			return Response(toreturn, mimetype="text/html")
		else:
			return redirect("/")
