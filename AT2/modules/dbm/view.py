import pymongo
import sys
import datetime
#import urllib2
import json
#from pymongo import ObjectId
from bson import ObjectId
connection = pymongo.MongoClient("mongodb://localhost")
db = connection.attendance_tracker_db
courses = db.courses
students = db.students
staff = db.staff
classes = db.classes

def allCourses():
	try:
		cursor = courses.find({})
		return list(cursor)
	except Exception as e:
		return "Error viewing all courses! <e>"+str(e)+"</e>"

def specificCourse(courseCode):
	try:
		course = courses.find_one({"courseCode":courseCode})
		return str(course["name"]), str(course["lecturer"]), course["students"]
	except Exception as e:
		return "Learning to deal with Errors 101 - "+str(e), "-1", []

def allStudents():
        try:
                cursor = students.find({})
                return list(cursor)
        except Exception as e:
                return "Error viewing all students! <e>"+str(e)+"</e>"

def specificStudent(studentNumber):
        try:
                student = students.find_one({"studentNumber":studentNumber})
                #student = students.find({"_id": ObjectId(studentNumber)})
                return str(student["firstname"]), str(student["lastname"])
        except Exception as e:
                return str(e), "Generator"
def allStaff():
        try:
                cursor = staff.find({})
                return list(cursor)
        except Exception as e:
                return "Error viewing all staff members! <e>"+str(e)+"</e>"

def specificStaffMember(staffNumber):
        try:
                staffmember = staff.find_one({"staffNumber":staffNumber})
                return str(staffmember["firstname"]), str(staffmember["lastname"]), staffmember["admin"]
        except Exception as e:
                return str(e), "Generator", "-1"

