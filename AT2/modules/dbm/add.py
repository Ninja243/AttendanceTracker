import pymongo
import sys
import datetime
#import urllib2
import json
# Hash later

connection = pymongo.MongoClient('mongodb://localhost')
db = connection.attendance_tracker_db
courses = db.courses
students = db.students
staff = db.staff

# Add course
#def course(couseCode, lecturers):

def course(courseCode, lecturer, courseName):
	try:
		courses.insert_one({
			"courseCode": courseCode,
			"name": courseName,
			"lecturer": lecturer,
			"students": []
		})
	except Exception as e:
		if courses.find_one({"courseCode":courseCode}):
			return "Course already exists!"
		return "Error adding course! <e>"+str(e)+"</e>"

def student(studentNumber, FirstName, LastName, password):
	try:
		students.insert_one({
			"studentNumber": studentNumber,
			"firstname": FirstName,
			"lastname": LastName,
			"password": password
		})
	except Exception as e:
		if students.find_one({"studentNumber":studentNumber}):
			return "Student already exists!"
		return "Error adding student! <e>"+str(e)+"</e>"

def staffMember(staffNumber, FirstName, LastName, admin, password):
	try:
		staff.insert_one({
			"staffNumber": staffNumber,
			"firstname": FirstName,
			"lastname": LastName,
			"admin": admin,
			"password": password
		})
	except Exception as e:
		if staff.find_one({"staffNumber":staffNumber}):
			return "Staff member already exists!"
		return "Error adding staff member! <e>"+str(e)+"</e>"

def studentToCourse(studentNumber, courseCode):
	try:
		student = students.find_one({"studentNumber":studentNumber})
		course = courses.find_one({"studentNumber":courseCode})
		course['students'] = course['students']+studentNumber
		return "Student added to course!"
	except Exception as e:
		if not students.find_one({"studentNumber":studentNumber}):
			return "No such student exists! <e>"+str(e)+"</e>"
		if not courses.find_one({"courseCode":courseCode}):
			return "No such course exists! <e>"+str(e)+"</e>"
		return "Error adding student to course! <e>"+str(e)+"</e>"
