import pymongo
import sys
import datetime

connection  = pymongo.MongoClient("mongodb://localhost")
db = connection.attendance_tracker_db
courses = db.courses
staff = db.staff
students = db.students

def allCourses():
	try:
		courses.remove({})
		if courses.find_one():
			return "Error removing courses"
		else:
			return "All courses removed"
	except Exception as e:
		return "Error removing courses! <e>"+str(e)+"</e>"

def allStaff():
	try:
		staff.remove({})
		if staff.find_one():
			return "Error removing staff members"
		else:
			return "All staff members removed!"
	except Exception as e:
		return "Error removing staff members! <e>"+str(e)+"</e>"

def allStudents():
	try:
		students.remove({})
		if students.find_one():
			return "Error removing students"
		else:
			return "All students removed!"
	except Exception as e:
		return "Error removing students! <e>"+str(e)+"</e>"
