import pymongo
import sys
import datetime
import json

connection = pymongo.MongoClient('mongodb://localhost')
db = connection.attendance_tracker_db
staff = db.staff
students = db.students

def login(ID, password):
    try:
        if students.find_one({"studentNumber":ID}):
            if students.find_one({"studentNumber":ID})['password'] == password:
                 return "Logged in student"
        elif staff.find_one({"staffNumber":ID}):
            if staff.find_one({"staffNumber":ID})['password'] == password:
                 return "Logged in staff"
        return "Incorrect username or password"
    except Exception as e:
        print(str(e))
        return "Incorrect username or password"
