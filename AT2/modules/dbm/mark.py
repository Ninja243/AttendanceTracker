import pymongo
import sys
import datetime
#import urllib2
import json

connection = pymongo.MongoClient('mongodb://localhost')
db = connection.attendance_tracker_db
courses = db.courses
students = db.students
classes = db.classes



def studentPresent(courseCode, studentNumber, classNumber):
	try:
		#course = courses.find_one({"courseCode":courseCode})
		#student = courses.find_one({"studentNumber":studentNumber})
		#course['classes'] = course['classes'] + (classNumber,datetime.datetime.now(), studentNumber)
		spclass = classes.insert_one({"classNumber":classNumber, "courseCode":courseCode, "studentNumber":studentNumber})
		return "Student marked present!"
	except Exception as e:
		if not courses.find_one({"courseCode":courseCode}):
			return "No such class exists! <e>"+str(e)+"</e>"
		if not students.find_one({"studentNumber":studentNumber}):
			return "No such student exists! <e>"+str(e)+"</e>"
		# The class might already exist, append student number to array
		#try:
		#	classes = courses.find_one({"_id":courseCode})['classes']
		#	classes[""]

#def studentAbsent(courseCode, studentNumber, classNumber):
#	try:
#		course = courses.find_one({"_id":courseCode})
#		student = courses.find_one({"_id":studentNumber})
#		course['']
#
